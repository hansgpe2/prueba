describe('Debe generar una compra', function() {
  before(function() {
    cy.visit('/')
  })
  it('Debe seleccionar el producto', function() {
    /* ==== Generated with Cypress Studio ==== */
    cy.get(':nth-child(1) > .card > .card-block > .card-title > .hrefch').click();
    cy.get('.name').click();
    cy.get('.name').click();
    cy.get('.name').click();
    cy.get('.name').as('producto')
    cy.get('.col-sm-12 > .btn').click();
  })

  it('Debe revisar si se encuentra en el carrito de compras', function() {
    cy.get('#cartur').click();
    cy.get('.success > :nth-child(2)').should('have.text', this.producto);
    cy.get('.col-lg-1 > .btn').click();
  });
  
  it('Se finaliza la compra', function() {
    cy.get('#name').clear();
    cy.get('#name').type('demo');
    cy.get('#country').clear();
    cy.get('#country').type('demo');
    cy.get('#city').clear();
    cy.get('#city').type('demo');
    cy.get('#card').clear();
    cy.get('#card').type('demo');
    cy.get('#month').clear();
    cy.get('#month').type('06');
    cy.get('#year').clear();
    cy.get('#year').type('12');
    cy.get('#orderModal > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click();
    cy.get('.sweet-alert > h2').should('have.text', 'Thank you for your purchase!');
    cy.get('.confirm').click();
    /* ==== End Cypress Studio ==== */
  })
})
